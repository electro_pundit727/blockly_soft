// selecting the analog input pin
const int inputPin = A0;
// size of the window
const int inputWindow = 100;
// placeholder for a single measurement
unsigned int inputSample;

void setup() {
  // initializing the analog input
  pinMode(inputPin, INPUT);
  // initializing the serial communication
  Serial.begin(9600);
}

void loop() {

  // two variables for minimum and maximum values in window
  unsigned int inputMax = 0;
  unsigned int inputMin = 1024;

  // loop for the window
  for (unsigned int i = 0; i < inputWindow; i++) {
    // read in a single value
    inputSample = analogRead(inputPin);   
    // get the minimum and maximum value
    inputMin = min(inputMin, inputSample);
    inputMax = max(inputMax, inputSample);
  }

  // send the values on serial
  Serial.print("Min: ");
  Serial.print(inputMin);
  Serial.print("  Max: ");
  Serial.print(inputMax);
  Serial.print("  Diff: ");
  Serial.print(inputMax - inputMin);
  Serial.println();
}
