#include <dht.h>

#define dht_apin A0     //Analog pin sensor is connected to

dht DHT;

void setup(){

  Serial.begin(9600);
  delay(500);    //Delay to system boot
  
}  

void loop(){

  DHT.read11(dht_apin);
  Serial.print("Current humidity = ");
  Serial.print(DHT.humidity);
  Serial.print("% ");
  Serial.print("Current temperature = ");
  Serial.print(DHT.temperature);
  Serial.println("C ");
  delay(2000);
    
}

