const int led = 3; // variable which stores pin number

void setup()
{
  Serial.begin(9600);
  pinMode(led, OUTPUT);  //configures pin 3 as OUTPUT
}

void loop()
{
  int sensor_value = analogRead(A0);
  Serial.println(sensor_value);

  if (sensor_value < 150)// the point at which the state of LEDs change
  {
    digitalWrite(led, HIGH);  //sets LEDs ON
  }
  else
  {
    digitalWrite(led, LOW); //Sets LEDs OFF
  }

  delay(10); //short delay for faster response to light
}
